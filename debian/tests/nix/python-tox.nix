{ pkgs ? import <nixpkgs> {}, py-ver ? "11" }:
let
  python-name = "python3${py-ver}";
  python = builtins.getAttr python-name pkgs;
  python-pkgs = python.withPackages (p: with p; [ tox ]);
in pkgs.mkShell {
  buildInputs = [ python-pkgs ];
  shellHook = ''
    set -e
    TOX_SKIP_ENV=runtime python3.${py-ver} -m tox -p all
    tox -e runtime
    exit
  '';
}
